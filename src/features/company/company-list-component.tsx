import React from 'react';

type Props = {
    data: [],
    ClickHandler: (event: React.MouseEvent<HTMLButtonElement>) => void
}

const CompanyListComponent = (listProps: Props) => {

    function greetUser() {
        console.log("Hi there, user!");
    }

    return (
        <div className="page-header section-height-50"
             style={{backgroundImage: `url('https://images.unsplash.com/photo-1564069114553-7215e1ff1890?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=2389&amp;q=80')`}}>
            <span className="mask bg-gradient-dark opacity-6"/>
            <div className="position-absolute w-100 z-index-1 bottom-0">
                <svg className="waves" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
                     viewBox="0 24 150 40" preserveAspectRatio="none" shapeRendering="auto">
                    <defs>
                        <path id="gentle-wave"
                              d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"/>
                    </defs>
                    <g className="moving-waves">
                        <use xlinkHref="#gentle-wave" x="48" y="-1" fill="rgba(255,255,255,0.40"/>
                        <use xlinkHref="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.35)"/>
                        <use xlinkHref="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.25)"/>
                        <use xlinkHref="#gentle-wave" x="48" y="8" fill="rgba(255,255,255,0.20)"/>
                        <use xlinkHref="#gentle-wave" x="48" y="13" fill="rgba(255,255,255,0.15)"/>
                        <use xlinkHref="#gentle-wave" x="48" y="16" fill="rgba(255,255,255,1"/>
                    </g>
                </svg>
            </div>
            <div className="container">
                <div className="row blur shadow-blur mt-n6 border-radius-md pb-4 p-3 mx-sm-0 mx-1 position-relative">
                    <div className="col-lg-3 mt-lg-n2 mt-2">
                        <label>Libellé dans l'annonce</label>
                        <div className="input-group">
                            <span className="input-group-text">
                                <i className="fas fa-file" aria-hidden="true"/>
                            </span>
                            <input className="form-control datepicker" placeholder="annonce contient le mot ex: java"
                                   type="text"/>
                        </div>
                    </div>
                    <div className="col-lg-3 mt-lg-n2 mt-2">
                        <label>Date d'apparaition</label>
                        <div className="input-group">
                            <span className="input-group-text">
                                <i className="fas fa-calendar" aria-hidden="true"/>
                            </span>
                            <input className="form-control datepicker" placeholder="Please select date" type="text"/>
                        </div>
                    </div>
                    <div className="col-lg-3 d-flex align-items-center my-auto">
                        <button type="button" className="btn bg-gradient-info w-100 mb-0 m4 mt-3">Rechercher</button>
                    </div>
                    <div className="col-lg-3 d-flex align-items-center my-auto">
                        <button type="button" className="btn bg-gradient-dark w-100 mb-0 m4 mt-3"
                                onClick={listProps.ClickHandler}>Nouvelle annonce
                        </button>
                    </div>
                </div>
            </div>
        </div>


    );
}

export default CompanyListComponent;