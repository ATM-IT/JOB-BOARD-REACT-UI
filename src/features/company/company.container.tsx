import React from 'react';
import CompanyHeaderComponent from './company-header-component';
import '../../scss/soft-design-system.scss';
import CompanyListComponent from './company-list-component';
import NewEditCompanyWorkaddComponent from './new-edit-company-workadd-component';

type CompanyContainerProps = {
    showModal: boolean
}

type CompanyContainerState = {
    showModal: boolean
}

export class CompanyContainer extends React.Component<CompanyContainerProps, CompanyContainerState> {

    constructor(props: CompanyContainerProps) {
        super(props);
        this.toggleClickHandler = this.toggleClickHandler.bind(this);
        this.state = {showModal: false};
    }

    toggleClickHandler = () => {
        console.log("toggle");
        // this.props.showModal = !this.props.showModal;
        this.setState({
            showModal: !this.state.showModal
        })
    }

    render() {
        return (
            <>
                <CompanyHeaderComponent msg={"COUOCU"}/>
                <CompanyListComponent data={[]} ClickHandler={this.toggleClickHandler}/>
                <NewEditCompanyWorkaddComponent onHide={this.toggleClickHandler} show={this.state.showModal}/>
            </>

        );
    }
}