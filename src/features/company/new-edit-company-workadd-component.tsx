import React from 'react';
import { Button, Modal } from 'react-bootstrap';

type Props = {
    onHide: (event: React.MouseEvent<HTMLButtonElement>) => void
    show : boolean;
}

const NewEditCompanyWorkaddComponent = (props: Props) => {

    return (
        <Modal
            {...props}
            show={props.show}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Modal heading
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form role="form">
                    <input type="hidden" name="documentID"/>
                    <div className="card" style={{boxShadow: "none"}}>
                        <div className="card-header"><h3 className="mb-0">Informations générales</h3></div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <div className="input-group mb-4">
                                <span className="input-group-text">
                                        <span className="fas fa-suitcase"/>
                                </span>
                                            <input className="form-control"
                                                   placeholder="Intitulé du poste"
                                                   type="text"
                                                   name="title"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className=" row">
                                <div className=" col-md-6">
                                    <div className=" form-group">
                                        <div className=" input-group mb-4">
                                <span className=" input-group-text">
                                   <span className=" fas fa-envelope"/>
                                </span>
                                            <input className="form-control"
                                                   placeholder=" Adresse email"
                                                   type="email" name="email"/>
                                        </div>
                                    </div>
                                </div>
                                <div className=" col-md-6">
                                    <div className=" form-group">
                                        <div className=" input-group mb-4">
                                <span className=" input-group-text">
                                    <span className=" fas fa-phone"/>
                                </span>
                                            <input className="form-control"
                                                   placeholder="Numéro de téléphone"
                                                   type="text" name="phoneNumber"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className=" row">
                                <div className=" col-md-6">
                                    <div className=" form-group">
                                        <div className=" input-group mb-4">
                                <span className="input-group-text">
                                    <span className="fas fa-map-marker"/>
                                </span>
                                            <input className="form-control"
                                                   placeholder=" Localisation" type="text"
                                                   name="location"/>

                                        </div>
                                    </div>
                                </div>
                                <div className=" col-md-6">
                                    <div className=" form-group">
                                        <div className=" input-group mb-4">
                                <span className=" input-group-text">
                                    <span className=" fas fa-home"/>
                                </span>
                                            <input className="form-control"
                                                   placeholder=" Télé-Travail"
                                                   type="text" name=" teleCommuting"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className=" row">
                                <div className=" col-12">
                                    <div className=" form-group">
                            <textarea className="form-control"
                                      placeholder="Description du profil recherché"
                                      id="message-5"
                                      name="description"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default NewEditCompanyWorkaddComponent;