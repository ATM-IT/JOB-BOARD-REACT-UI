import React from 'react';

// the clock's state has one field: The current time, based upon the
// JavaScript class Date
type ClockState = {
    time: Date
}

type NoticeProps = {
    msg: string
}

// Clock has no properties, but the current state is of type ClockState
// The generic parameters in the Component typing allow to pass props
// and state. Since we don't have props, we pass an empty object.
export class Sidebar extends React.Component<NoticeProps> {

    static defaultProps = {
        msg: 'Hello everyone!'
    }

    render() {
        return <h1>{this.props.msg} Hello tous le monde </h1>;
    }
}