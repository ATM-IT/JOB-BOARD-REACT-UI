import { applyMiddleware, combineReducers, createStore, Middleware, Reducer, Store } from 'redux';
import thunkMiddleWare from 'redux-thunk';
import * as reducers from './reducers/AuthReducer';
import logger from 'redux-logger';

const appReducer = combineReducers(reducers);

const middlewares: Middleware[] = [thunkMiddleWare];
if (process.env.NODE_ENV === 'development') {
    middlewares.push(logger);
}

type MyStore = Store & { asyncReducers: { [key: string]: Reducer } };
const store: MyStore = createStore(appReducer, applyMiddleware(...middlewares)) as MyStore;

store.asyncReducers = {};
const createInjectReducer = (store: MyStore) => (
    key: string,
    reducer: Reducer
) => {
    store.asyncReducers[key] = reducer;
    store.replaceReducer(
        combineReducers({...reducers, ...store.asyncReducers})
    );
};

export const injectReducer = createInjectReducer(store);

export default store;