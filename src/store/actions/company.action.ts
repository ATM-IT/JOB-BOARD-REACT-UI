import { type } from '../../core/utils/type-check.helper';


export const ActionTypes = {
    SET_COMPANY_JOB_CREATE_FORM_DATA: type('[COMPANY_JOB] set data form of creation job in store'),

    ADD_COMPANY_JOB: type('[COMPANY_JOB] add company job'),
    ADD_COMPANY_JOB_SUCCESS: type('[COMPANY_JOB] add company job success'),
    ADD_COMPANY_JOB_ERROR: type('[COMPANY_JOB] add company error'),

    GET_COMPANY_JOB_LIST: type('[COMPANY_JOB] get company job list action'),
    GET_COMPANY_JOB_LIST_SUCCESS: type('[COMPANY_JOB] get company job list action success'),
    GET_COMPANY_JOB_LIST_ERROR: type('[COMPANY_JOB] get company job list action error'),

    DELETE_COMPANY_JOB: type('[COMPANY_JOB] delete company job  action'),
    DELETE_COMPANY_JOB_SUCCESS: type('[COMPANY_JOB] delete company job  action success'),
    DELETE_COMPANY_JOB_ERROR: type('[COMPANY_JOB] delete company job  action error'),

    SET_COMPANY_PROFILE_FORM_DATA: type('[COMPANY_PROFILE] set company profile data form'),

    GET_COMPANY_PROFILE: type('[COMPANY_PROFILE] get company profile'),
    GET_COMPANY_PROFILE_SUCCESS: type('[COMPANY_PROFILE] get company profile success '),
    GET_COMPANY_PROFILE_ERROR: type('[COMPANY_PROFILE] get company profile error '),

    ADD_COMPANY_PROFILE: type('[COMPANY_PROFILE] add company profile'),
    ADD_COMPANY_PROFILE_SUCCESS: type('[COMPANY_PROFILE] add company profile success'),
    ADD_COMPANY_PROFILE_ERROR: type('[COMPANY_PROFILE] add company profile error'),
};


