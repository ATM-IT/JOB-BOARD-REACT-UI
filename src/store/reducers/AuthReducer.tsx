import { Action } from 'redux';

export interface AuthState {
    authenticated: boolean;
    token: string;
    error: string;
    loaded: boolean;
    loading: boolean;
}

function initState() {
    return {
        authenticated: false,
        token: "",
        error: "",
        loaded: false,
        loading: false,
    } as AuthState
}

export const authReducer = (
    state: AuthState = initState(),
    action: Action
): AuthState => {
    return state;
};


