import React from 'react';
import './App.scss';
import { Route, Switch } from 'react-router-dom';
import { SplashScreen } from './features/splash-screen/SplashScreen';
import { CompanyContainer } from './features/company/company.container';

const NotFound = () => <h2>NotFound</h2>;

function App() {
    return (

        <Switch>
            <Route path="/splash" component={SplashScreen}/>
            <Route path="/company" component={CompanyContainer}/>
            <Route component={NotFound}/>
        </Switch>

    );
}

export default App;
